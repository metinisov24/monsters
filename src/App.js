import React, {
  Component
} from 'react';
import {
  CardList
} from './components/card-list/card-list.component'
import {SearchBox} from './components/seach.box-component/search-box.component'
import './App.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      string: 'Hello Yihua',
      monsters: [],
      searchField: ''
    }

   
  }

  componentDidMount() {
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(response => response.json())
      .then(users => this.setState({
        monsters: users
      }))
  }

  handleChange = (e)=>{
    this.setState({
      searchField: e.target.value}
    )
}
  render() {
    const {monsters, searchField} = this.state;
    const filteredMonsters = monsters.filter(monster => monster.name.toLowerCase().includes(searchField.toLowerCase()))

    return ( <div className = "App" >
      <h1 > Monster Rolodex < /h1> 
     <SearchBox 
     placeholder="Search M"
     handleChange={this.handleChange}
     />
        <CardList monsters ={filteredMonsters}>

        </CardList>


        </div>
      )
    }
  }

  export default App;